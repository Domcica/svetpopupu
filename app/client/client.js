// insert in <script> tags before closing </body> tag or link externally
let quotes = [
    {id: 1, author: "Michel Houllebecq", quote: "I was incapable of living for myself, and who else did I have to live for?", highlight: "incapable"},
    {id: 2, author: "ja", quote: "Ništ.", highlight: "Ništ."},
    {id: 3, author: "Zygmunt Bauman", quote: "Lidské bytosti přirozeně tíhnou k přehlednosti, nesnášíme rozpornost, nejasnost. Cítíme se v ní nepohodlně, nejistě a v jistém vágním smyslu ohroženě.", highlight: "ohroženě"},
    {id: 4, author: "Platón", quote: "Sebekázeň je společnicí samoty", highlight: "samoty"},
    {id: 5, author: "Martin Heidegger", quote: "Pobyt je jsoucno, o kterém nelze říci, že se mezi jiným jsoucnem jen vyskytuje. Toto jsoucno se onticky vyznačuje spíše tím, že mu v jeho bytí o toto bytí samo jde.", highlight: "jsoucno"},
    {id: 6, author: "Martin Heidegger", quote: "Existence je dobrodružství své vlastní nemožnosti.", highlight: "nemožnosti"},
    {id: 7, author: "Georg Wilhelm Friedrich Hegel", quote: "Naprostá svoboda nemůže stvořit žádné kladné dílo ani čin, zůstává jí pouze záporné konání, je to pouhý běs zanikání.", highlight: "záporné konání"},
    {id: 8, author: "Cormac McCarthy", quote: "“The point is there ain't no point.", highlight: "ain't"},
    {id: 9, author: "Karel Čapek", quote: "Každý člověk by měl něco hledat.", highlight: "hledat"},
    {id: 10, author: "Matka Tereza", quote: "Umřít znamená vrátit se domů.", highlight: "Umřít"},
    {id: 11, author: "Immanuel Kant", quote: "Rozum není udělán k tomu, aby se izoloval, nýbrž aby vstupoval do společenství. Reflexionen zur Anthropologie, Nr. 897, AA XV.392.", highlight: "izoloval"},
    {id: 12, author: "Chuck Palahniuk", quote: "If you could be either God’s worst enemy or nothing, which would you choose?", highlight: "enemy"},
    {id: 13, author: "A.A. Milne", quote: "There must be somebody there, because somebody must have said 'Nobody.", highlight: "nobody"},
    {id: 14, author: "Albert Camus", quote: "If we believe in nothing, if nothing has any meaning and if we can affirm no values whatsoever, then everything is possible and nothing has any importance.", highlight: "nothing"},
    {id: 15, author: "Alan Moore", quote: "Why do we argue? Life's so fragile, a successful virus clinging to a speck of mud, suspended in endless nothing.", highlight: "virus"},
    {id: 16, author: "Michel De Montaigne", quote: " Přemýšlení o smrti je přemýšlením o svobodě.", highlight: "smrti"},
    {id: 17, author: "Albert Camus", quote: "Tento svět není důležitý a kdo to pozná, získá svobodu.", highlight: "není důležitý"},
    {id: 18, author: "Sigmund Freud ", quote: "Každý z nás dluží přírodě svoji smrt.", highlight: "smrt"},
    {id: 19, author: "Friedrich Nietzsche", quote: "Život – dlouhá to smrt.", highlight: "smrt"},
    {id: 20, author: "Eugène Ionesco", quote: "That's how we stay young these days: murder and suicide.", highlight: "murder"},
    {id: 21, author: "Flannery O'Connor", quote: "“If you live today, you breath in nihilism ... it's the gas you breathe.", highlight: "nihilism"},
    {id: 22, author: "Gorgias", quote: "Nothing exists; even if something exists, nothing can be known about it; and even if something can be known about it, knowledge about it can't be communicated to others.", highlight: "nothing"},
    {id: 23, author: "Emil Cioran", quote: "“There is no other world. Nor even this one. What, then, is there? The inner smile provoked in us by the patent nonexistence of both.", highlight: "no other"},
    {id: 24, author: "Irvine Welsh", quote: "“I like the idea of a black sun; like a black hole in space, sucking everything into darkness, where we came from and where we're heading", highlight: "black sun"},
    {id: 25, author: "Arthur Balfour", quote: "“Nothing matters very much and most things don't matter at all.", highlight: "nothing"},
    {id: 26, author: "Werner Herzog", quote: "“Civilization is like a thin layer of ice upon a deep ocean of chaos and darkness.", highlight: "chaos"},
    {id: 27, author: "Marty Rubin", quote: "“The nihilist sees a void. I see everything that fills it.", highlight: "void"},
    {id: 28, author: "Albert Camus", quote: "“Perhaps it is untrue to say that life is a perpetual choice. But it is true that it is impossible to imagine a life deprived of all choice.", highlight: "impossible"},
    {id: 29, author: "Alan Moore", quote: "The horror is this: In the end, it is simply a picture of empty meaningless blackness. We are alone. There is nothing else.", highlight: "blackness"},
    {id: 30, author: "Karl Ove Knausgaard", quote: "“Dostoyevsky has become a teenager’s writer, the issue of nihilism a teenage issue.", highlight: "nihilism"},
    {id: 31, author: "Rajesh", quote: "“Nothing exists (thus the proof for that also cannot exist).", highlight: "nothing"},
    {id: 32, author: "Epic of Gilgamesh", quote: "“As for man, his days are numbered, whatever he may do, it is but wind.", highlight: "wind"},
    {id: 33, author: "Anupam S Shlok", quote: "“Humans are born to die, anything in between is just pure nonsense.", highlight: "nonsense"},
    {id: 34, author: "Iris Murdoch", quote: "The world is perhaps ultimately to be defined as a place of suffering. Man is a suffering animal, subject to ceaseless anxiety and pain and fear.", highlight: "a place of suffering"},
    {id: 35, author: "John Gardner", quote: "The trees are dead. The days are an arrow in a dead man's chest.", highlight: "are dead"},
    {id: 36, author: "Iris Murdoch", quote: "Perhaps in the end the suffering is all, it's all contained in the suffering. The final atoms of it all are simply pain.", highlight: "suffering"},
    {id: 37, author: "Mladen Đorđević", quote: "The hunger for believing in good things, usually turns up as starvation.", highlight: "starvation"},
    {id: 38, author: "Vizi Andrei", quote: "From a cosmic perspective, I'm simply an accident that has nothing to lose. Why take it all so seriously?", highlight: "lose"},
    {id: 39, author: "Willem Frederik Hermans", quote: "Creative nihilism, aggressive pity, total misanthropy.", highlight: "misanthropy"},
    {id: 40, author: "Rollo May", quote: "Science, Nietzsche had warned, is becoming a factory, and the result will be ethical nihilism.", highlight: "nihilism"},
];
let styleTypes = ["style-light", "style-dark", "style-black", "style-grey", "style-blue"];
let currentQuote;
function randomInt(max) {
    let randomInt = Math.floor(Math.random() * max);
    return randomInt
}
function getHighlight(index) {
    let highlight = quotes[index].highlight;
    let splited = quotes[index].quote.split(highlight);
    return {highlight: highlight, splited: splited}
}
function insertSpan(index) {
    let highlightQuote = getHighlight(index).highlight;
    return "<span class='sv-quote__highlight'> "+highlightQuote+" </span>"
}
function createPopup(quotesArray, index) {
    let maxLength = quotesArray.length-1;
    let quoteIndex = randomInt(maxLength);
    popupTexts[index].innerHTML = '' + getHighlight(quoteIndex).splited[0] + insertSpan(quoteIndex) + getHighlight(quoteIndex).splited[1] + '';
    popupAuthor[index].innerHTML = '' + quotes[quoteIndex].author + '';
}
let popupTexts = document.querySelectorAll(".sv-pop .sv-pop__quote");
let popupAuthor = document.querySelectorAll(".sv-pop .sv-pop__author");
let popups = document.querySelectorAll(".sv-pop");

function generatePopups() {
    for (let i = 0; i < popups.length; i++) {
        popups[i].classList.remove('roll');
        setTimeout(function () {
            popups[i].classList.forEach(htmlClass=>{
                if(htmlClass.startsWith('style-')) {
                    popups[i].classList.remove(htmlClass);
                }
            });
            createPopup(quotes, i, quotes.length-1);
            popups[i].classList.add(styleTypes[randomInt(styleTypes.length)]);
            popups[i].classList.add('roll');
        }, 2000);
    }
}
window.addEventListener('load', function() {
    setTimeout(function () {
        for (let i = 0; i < popups.length; i++) {
            popups[i].classList.add('roll');
            popups[i].classList.add(styleTypes[randomInt(styleTypes.length)]);
            createPopup(quotes, i);
            currentQuote = i;
        }
        setInterval(generatePopups, 30000);
    }, 4500)
});
