let quotes = [
    {id: 1, author: "Michel Houllebecq", quote: "I was incapable of living for myself, and who else did I have to live for?", highlight: "incapable"},
    {id: 2, author: "ja", quote: "Ništ.", highlight: "Ništ."},
    {id: 3, author: "Zygmunt Bauman", quote: "Lidské bytosti přirozeně tíhnou k přehlednosti, nesnášíme rozpornost, nejasnost. Cítíme se v ní nepohodlně, nejistě a v jistém vágním smyslu ohroženě.", highlight: "ohroženě"},
    {id: 4, author: "Platón", quote: "Sebekázeň je společnicí samoty", highlight: "samoty"},
    {id: 5, author: "Martin Heidegger", quote: "Pobyt je jsoucno, o kterém nelze říci, že se mezi jiným jsoucnem jen vyskytuje. Toto jsoucno se onticky vyznačuje spíše tím, že mu v jeho bytí o toto bytí samo jde.", highlight: "jsoucno"},
    {id: 6, author: "Martin Heidegger", quote: "Existence je dobrodružství své vlastní nemožnosti.", highlight: "nemožnosti"},
    {id: 7, author: "Georg Wilhelm Friedrich Hegel", quote: "Naprostá svoboda nemůže stvořit žádné kladné dílo ani čin, zůstává jí pouze záporné konání, je to pouhý běs zanikání.", highlight: "záporné konání"},
    {id: 8, author: "Cormac McCarthy", quote: "“The point is there ain't no point.", highlight: "ain't"},
    {id: 9, author: "Karel Čapek", quote: "Každý člověk by měl něco hledat.", highlight: "hledat"},
    {id: 10, author: "Matka Tereza", quote: "Umřít znamená vrátit se domů.", highlight: "Umřít"},
    {id: 11, author: "Immanuel Kant", quote: "Rozum není udělán k tomu, aby se izoloval, nýbrž aby vstupoval do společenství. Reflexionen zur Anthropologie, Nr. 897, AA XV.392.", highlight: "izoloval"},
    {id: 12, author: "Chuck Palahniuk", quote: "If you could be either God’s worst enemy or nothing, which would you choose?", highlight: "enemy"},
    {id: 13, author: "A.A. Milne", quote: "There must be somebody there, because somebody must have said 'Nobody.", highlight: "nobody"},
    {id: 14, author: "Albert Camus", quote: "If we believe in nothing, if nothing has any meaning and if we can affirm no values whatsoever, then everything is possible and nothing has any importance.", highlight: "nothing"},
    {id: 15, author: "Alan Moore", quote: "Why do we argue? Life's so fragile, a successful virus clinging to a speck of mud, suspended in endless nothing.", highlight: "virus"},
    {id: 16, author: "Michel De Montaigne", quote: " Přemýšlení o smrti je přemýšlením o svobodě.", highlight: "smrti"},
    {id: 17, author: "Albert Camus", quote: "Tento svět není důležitý a kdo to pozná, získá svobodu.", highlight: "není důležitý"},
    {id: 18, author: "Sigmund Freud ", quote: "Každý z nás dluží přírodě svoji smrt.", highlight: "smrt"},
    {id: 19, author: "Friedrich Nietzsche", quote: "Život – dlouhá to smrt.", highlight: "smrt"},
    {id: 20, author: "Eugène Ionesco", quote: "That's how we stay young these days: murder and suicide.", highlight: "murder"},

    {id: 21, author: "Flannery O'Connor", quote: "“If you live today, you breath in nihilism ... it's the gas you breathe.", highlight: "nihilism"},
    {id: 22, author: "Gorgias", quote: "Nothing exists; even if something exists, nothing can be known about it; and even if something can be known about it, knowledge about it can't be communicated to others.", highlight: "nothing"},
    {id: 23, author: "Emil Cioran", quote: "“There is no other world. Nor even this one. What, then, is there? The inner smile provoked in us by the patent nonexistence of both.", highlight: "no other"},
    {id: 24, author: "Irvine Welsh", quote: "“I like the idea of a black sun; like a black hole in space, sucking everything into darkness, where we came from and where we're heading", highlight: "black sun"},
    {id: 25, author: "Arthur Balfour", quote: "“Nothing matters very much and most things don't matter at all.", highlight: "nothing"},
    {id: 26, author: "Werner Herzog", quote: "“Civilization is like a thin layer of ice upon a deep ocean of chaos and darkness.", highlight: "chaos"},
    {id: 27, author: "Marty Rubin", quote: "“The nihilist sees a void. I see everything that fills it.", highlight: "void"},
    {id: 28, author: "Albert Camus", quote: "“Perhaps it is untrue to say that life is a perpetual choice. But it is true that it is impossible to imagine a life deprived of all choice.", highlight: "impossible"},
    {id: 29, author: "Alan Moore", quote: "The horror is this: In the end, it is simply a picture of empty meaningless blackness. We are alone. There is nothing else.", highlight: "blackness"},

    {id: 30, author: "Karl Ove Knausgaard", quote: "“Dostoyevsky has become a teenager’s writer, the issue of nihilism a teenage issue.", highlight: "nihilism"},
    {id: 31, author: "Rajesh", quote: "“Nothing exists (thus the proof for that also cannot exist).", highlight: "nothing"},
    {id: 32, author: "Epic of Gilgamesh", quote: "“As for man, his days are numbered, whatever he may do, it is but wind.", highlight: "wind"},
    {id: 33, author: "Anupam S Shlok", quote: "“Humans are born to die, anything in between is just pure nonsense.", highlight: "nonsense"},
    {id: 34, author: "Iris Murdoch", quote: "The world is perhaps ultimately to be defined as a place of suffering. Man is a suffering animal, subject to ceaseless anxiety and pain and fear.", highlight: "a place of suffering"},
    {id: 35, author: "John Gardner", quote: "The trees are dead. The days are an arrow in a dead man's chest.", highlight: "are dead"},
    {id: 36, author: "Iris Murdoch", quote: "Perhaps in the end the suffering is all, it's all contained in the suffering. The final atoms of it all are simply pain.", highlight: "suffering"},
    {id: 37, author: "Mladen Đorđević", quote: "The hunger for believing in good things, usually turns up as starvation.", highlight: "starvation"},
    {id: 38, author: "Vizi Andrei", quote: "From a cosmic perspective, I'm simply an accident that has nothing to lose. Why take it all so seriously?", highlight: "lose"},
    {id: 39, author: "Willem Frederik Hermans", quote: "Creative nihilism, aggressive pity, total misanthropy.", highlight: "misanthropy"},
    {id: 40, author: "Rollo May", quote: "Science, Nietzsche had warned, is becoming a factory, and the result will be ethical nihilism.", highlight: "nihilism"},
    ];
    // congratulations you win to die once
    // congrats you win mortality
    // you are 1000th visitor of ...
    // you won ...
    // congratulations

var mobileDetect = document.querySelector('.mobile__detect');

let styleTypes = ["style-light", "style-dark", "style-black", "style-grey", "style-blue"];
let currentQuote;

function isMobile() {
    const mobileVisible = getComputedStyle(mobileDetect).getPropertyValue('display');
    return mobileVisible === 'block';
}
function createParticles(amount, wrap) {
    for (let p = 0; p < amount; p++) {
        let particleSmall = document.createElement('div');
        particleSmall.className = 'c';
        wrap.append(particleSmall);
    }
}
// background particles
if (isMobile()) {
    let particleWrapMobile = document.querySelector('.background__particles--mobile .wrap');
    createParticles(70, particleWrapMobile)
} else {
    let particleWrap = document.querySelector('.background__particles .wrap');
    createParticles(200, particleWrap)
}



function randomInt(max) {
    let randomInt = Math.floor(Math.random() * max);
    return randomInt
}

function getHighlight(index) {
    let highlight = quotes[index].highlight;
    let splited = quotes[index].quote.split(highlight);
    return {highlight: highlight, splited: splited}
}

function insertSpan(index) {
    let highlightQuote = getHighlight(index).highlight;
    return "<span class='quote__highlight'> "+highlightQuote+" </span>"
}

function createPopup(quotesArray, index) {
    // vzdy musi byt unikatne slovo, musi sa tam vyskytovat iba raz
    let maxLength = quotesArray.length-1;
    let quoteIndex = randomInt(maxLength);
    popupTexts[index].innerHTML = '' + getHighlight(quoteIndex).splited[0] + insertSpan(quoteIndex) + getHighlight(quoteIndex).splited[1] + '';
    popupAuthor[index].innerHTML = '' + quotes[quoteIndex].author + '';
}


    // close when on starting/init page
let wrapper = document.querySelector(".wrapper");
let wrapperMenu = document.querySelector(".wrapper__menu");
var wrapperText = document.querySelector('.wrapper__text');
var previewPopup = document.querySelector('.wrapper__text .text__preview');
let popupTexts = document.querySelectorAll(".pop .pop__quote");
let popupAuthor = document.querySelectorAll(".pop .pop__author");
let popups = document.querySelectorAll(".pop");

window.addEventListener('load', function() {
    // init menu
    setTimeout(function () {
        wrapperMenu.classList.add('roll');
    }, 500)

    // init popupu
    setTimeout(function () {
        for (let i = 0; i < popups.length; i++) {
            popups[i].classList.add('roll');
            popups[i].classList.add(styleTypes[randomInt(styleTypes.length)]);
            createPopup(quotes, i);
            currentQuote = i;
        }
    }, 4500)
});


// generate more
function generatePopups() {
    for (let i = 0; i < popups.length; i++) {
        popups[i].classList.remove('roll');
        setTimeout(function () {
            popups[i].classList.forEach(htmlClass=>{
                if(htmlClass.startsWith('style-')) {
                    popups[i].classList.remove(htmlClass);
                }
            });
            createPopup(quotes, i, quotes.length-1);
            popups[i].classList.add(styleTypes[randomInt(styleTypes.length)]);
            popups[i].classList.add('roll');
        }, 2000);
    }
}
var buttonChange = document.querySelector('.button.change');
var buttonGetSet = document.querySelector('.button.getset');
var buttonGetCustom = document.querySelector('.button.getcustom');
var buttonGenerate = document.querySelector('.button.generate');
var buttonStart = document.querySelector('.button.start');

buttonChange.addEventListener('click', function(){
    generatePopups();
});

// create custom

var snippet = document.querySelector('.menu__custom .snippet');
var snippetCopy = document.querySelector(".snippet .snippet__copy");

buttonGetCustom.addEventListener('click', function() {
    wrapper.classList.add('custom');
    for (let i = 0; i < popups.length; i++) {
        popups[i].classList.remove('roll');
        popups[i].classList.add('unroll');
    }
});

// copy snippet button
function copyToClipboard(target) {
    var textArea = document.createElement("textarea");
    textArea.value = target
    document.body.appendChild(textArea);
    textArea.select();
    document.execCommand("Copy");
    textArea.remove();

}

// change color/input without refresh
const customControls = document.querySelector('.custom__controls');
var customBackground;
var customBorder;
var approvedColors = ['#323232', '#666666', '#999999', '#bbbbbb', '#ececec'];
customControls.addEventListener('change', (event) => {
    if (event.target.id ==='color-stroke') {
        const target = document.querySelector('.wrapper__text');
        if (!(approvedColors.includes(event['srcElement'].value))) {
            target.style.borderColor = '#000000';
            customBorder = '#000000';
        } else {
            target.style.borderColor = event['srcElement'].value;
            customBorder = event['srcElement'].value;
        }

    } else if (event.target.id === 'color-fill') {
        const target = document.querySelector('.wrapper__text');
        if (!(approvedColors.includes(event['srcElement'].value))) {
            target.style.background = '#000000';
            customBackground = '#000000';
        } else {
            target.style.background = event['srcElement'].value;
            customBackground = event['srcElement'].value;
        }
    }
});
function sanitize(string) {
  const map = {
      '&': '&amp;',
      '<': '&lt;',
      '>': '&gt;',
      '"': '&quot;',
      "'": '&#x27;',
      "/": '&#x2F;',
  };
  const reg = /[&<>"'/]/ig;
  return string.replace(reg, (match)=>(map[match]));
}
// generate snippet
buttonGenerate.addEventListener('click', function(){
    wrapper.classList.remove('custom');
    wrapper.classList.add('result');

    var inputTextCurrent = document.querySelector('.wrapper.result .text__custom textarea');
    var customText = document.querySelector('.wrapper.result .text__preview span');
    var sanitizedText = sanitize(inputTextCurrent.value);
    customText.textContent = inputTextCurrent.value;
    previewPopup.append(customText);

    function getCustom() {
        var xhr = new XMLHttpRequest();
        xhr.open("GET", 'http://localhost:8000/app/client/client_custom.html', true);
        xhr.send();
        xhr.onload = function () {
            var sourceContent = xhr.responseText;

            var replaceCssBase = '.sv-pop{background:#000000;border:3px solid #000000;';
            var replaceCss = replaceCssBase.replace('background:#000000', 'background:' + customBackground);
            replaceCss = replaceCss.replace('border:5px solid #000000;', 'border:3px solid ' + customBorder + ';');
            sourceContent = sourceContent.replace('.sv-pop{background:#000000;border:3px solid #000000;', replaceCss);

            sourceContent = sourceContent.replace( '__sv-quote-content__', sanitizedText);
            makeSetSnippet(sourceContent);
            return sourceContent
        };
    }
    snippetCopy.addEventListener('click', function(){
        getCustom();
        snippetCopy.classList.add('show');
    });

});

function getSource(type) {
    var xhr = new XMLHttpRequest();
    xhr.open("GET", 'http://localhost:8000/app/client/client.' + type, true);
    xhr.send();
    xhr.onload = function(){
        var sourceContent = xhr.responseText;
        makeSetSnippet(sourceContent);
    };
}

function makeSetSnippet(target) {
    copyToClipboard(target);
}
// get set
buttonGetSet.addEventListener('click', function() {
    wrapper.classList.add('set');
    var snippetHtml = document.querySelector('.wrapper.set .set__html');
    var snippetCss = document.querySelector('.wrapper.set .set__css');
    var snippetJs = document.querySelector('.wrapper.set .set__js');

    snippetHtml.addEventListener('click', function() {
        getSource('html');
        snippetHtml.classList.add('show');
    });
    snippetCss.addEventListener('click', function() {
        getSource('css');
        snippetCss.classList.add('show');
    });
    snippetJs.addEventListener('click', function() {
       getSource('js');
       snippetJs.classList.add('show');
    });
});


var inputText = document.querySelector('.wrapper .text__custom textarea');
var inputColors = document.querySelectorAll('.wrapper .menu__custom input');
function backToDefault() {
    wrapper.classList.remove('custom');
    wrapper.classList.remove('result');
    wrapper.classList.remove('set');
    inputText.value = '';
    for (let i = 0; i < popups.length; i++) {
        popups[i].classList.remove('unroll');
        popups[i].classList.add('roll');
    }
    unclosesPopup.forEach(function (unclosed){
        unclosed.classList.remove('turn');
    })
    inputColors.forEach(function(input){
        input.value = '#000000';
    });
    wrapperText.style.borderColor = "#000000";
    wrapperText.style.background = "#000000";
}
buttonStart.addEventListener('click', function(){
    backToDefault();
});

// close custom section - back to start
const closes = document.querySelectorAll('.close--menu');
closes.forEach(function(close) {
    close.addEventListener('click', function() {
        backToDefault();
    });
});

const closesPopup = document.querySelectorAll('.close--popup');
closesPopup.forEach(function(close) {
    close.addEventListener('click', function() {
        close.classList.add('turn')
    });
});

const unclosesPopup = document.querySelectorAll(".wrapper:not(.custom):not(.set) .close--menu");
console.log('unclosed', unclosesPopup);
unclosesPopup.forEach(function(unclose) {
    unclose.addEventListener('click', function() {
        unclose.classList.add('turn')
    });
});


