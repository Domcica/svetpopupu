let gulp = require('gulp');
let cleanCSS = require('gulp-clean-css');
let prefix = require('gulp-autoprefixer');
let concat = require('gulp-concat');
var sass = require('gulp-sass');
var log = require('fancy-log');

gulp.task('sass', function() {
       return gulp.src('./app/styles/*.scss')
        .pipe(sass())
        .pipe(prefix('last 2 versions'))
        .pipe(concat('style.min.css'))
        .pipe(cleanCSS())
        .pipe(gulp.dest('dist'));
});

/*gulp.task('rename', function() {
      var rename = require("gulp-rename");
      return gulp.src("app/style.css")
        .pipe(rename(function (path) {
        path.basename += ".min";
        }))
        .pipe(gulp.dest("./dist"));
});*/

/*gulp.task('delete', function() {
      var gulp = require('gulp');
      var clean = require('gulp-clean');
      return gulp.src('app/style.css', {read: false})
        .pipe(clean());
});*/

// gulp.task('build', gulp.series(['sass', 'rename', 'delete']));
gulp.task('build', gulp.series('sass'));

gulp.task('default', function() {
    gulp.watch('./app/styles/*.scss', gulp.series(['build']));
});

